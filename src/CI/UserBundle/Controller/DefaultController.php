<?php

namespace CI\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use CI\UserBundle\Entity\User;
use Symfony\Component\Form\Form;

class DefaultController extends Controller
{
    //Display the list of users
    public function indexAction()
    {
    	$repo = $this->getDoctrine()->getRepository("CIUserBundle:User");
    	$users = $repo->findAll();
        return $this->render('CIUserBundle:Default:index.html.twig', array('users' => $users));
    }

    //Insert form
    public function formAction()
    {
        $user = new User();
        $form   = $this->createFormBuilder($user)
                ->setAction($this->generateUrl('user_create'))
                ->add('name', 'text')
                ->add('email', 'email')
                ->add('password', 'password')
                ->add('address', 'text')
                ->add('save','submit')
                ->getForm();
        return $this->render("CIUserBundle:Default:form.html.twig",
            array(
                'form'=>$form->createView(),
                ));
    }
    
    //Update From
    public function editAction($id)
    {
        $repo = $this->getDoctrine()->getRepository("CIUserBundle:User");
        $user = $repo->find($id);
        return $this->render("CIUserBundle:Default:edit.html.twig", array('user'=> $user));
    }

    //Create a user!
    function createAction(Request $request)
    {
        $user = new User();
        $form   = $this->createFormBuilder($user)
                ->setAction($this->generateUrl('user_create'))
                ->add('name', 'text')
                ->add('email', 'email')
                ->add('password', 'password')
                ->add('address', 'text')
                ->add('save','submit')
                ->getForm();
        $form->handleRequest($request);
        if($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }
        //return $this->render('CIUserBundle:Default:test.html.twig', array('demo'=>$form));
    	return $this->redirect($this->generateUrl('user_list'));
    }

    //Show a user's information
    function showAction($id)
    {
    	$userRepo = $this->getDoctrine()->getRepository("CIUserBundle:User");
    	$user = $userRepo->find($id);
        return $this->render('CIUserBundle:Default:show.html.twig', array('user' => $user));
    }

    //Update the information of user
    function updateAction($id)
    {
        $request = Request::createFromGlobals();
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("CIUserBundle:User")->find($id);
        if(!$user)
        {
            throw $this->createNotFoundException('No User found for id '.$id);
        }
        $user->setName($request->request->get('name'));
        $user->setEmail($request->request->get('email'));
        $user->setPassword($request->request->get('password'));
        $user->setAddress($request->request->get('address'));
        $em->flush();
        $session = new Session();
        $session->getFlashBag()->add(
                'message', 'The information was updated successfully!'
            );
        return $this->redirect($this->generateUrl('user_list'));
    }

    //Delete a user
    function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("CIUserBundle:User")->find($id);
        if(!$user)
        {
            throw $this->createNotFoundException("The user is not available to delete");
        }
        $em->remove($user);
        $em->flush();

        $session = new Session();
        $session->getFlashBag()->add(
                'message', 'Deleted successfully!'
            );
        return $this->redirect($this->generateUrl('user_list'));
    }
}
